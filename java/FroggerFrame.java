import javax.swing.*;
import java.awt.*;

@SuppressWarnings("WeakerAccess")
public class FroggerFrame extends JFrame {
    public FroggerFrame() {
        super("Frogger");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        FroggerPanel p = new FroggerPanel();
        Insets frameInsets = getInsets();
        int frameWidth = p.getWidth() + (frameInsets.left + frameInsets.right);
        int frameHeight = p.getHeight() + (frameInsets.top + frameInsets.bottom);
        setPreferredSize(new Dimension(frameWidth, frameHeight));
        setLayout(null);
        add(p);
        pack();
        setVisible(true);
        Logger.logOtherMessage("Window", "Window Created.");
    }
}