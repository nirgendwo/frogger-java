import javax.swing.*;
import java.io.File;
import java.io.FileWriter;

@SuppressWarnings("ALL")
public class Logger {
    private static File f;

    public Logger() {
        f = new File("programLog.txt");
        Logger.logOtherMessage("New Run", "----------------------\n");
    }

    /**
     * logs a code message, such as a creation success.
     */
    public static void logCodeMessage(String message) {
        try {
            // FileWriter fw = new FileWriter(f, true); //the true will append the new data
            // fw.write("\n[Code] " + message);
            // fw.close();
        } catch (Exception e) {
            System.err.println("Error with writing logging file. " + e.getMessage());
        }
    }

    /**
     * logs input of the user in the console/gui, for recreation of bugs.
     */
    public static void logUserMessage(String message) {
        try {
            // FileWriter fw = new FileWriter(f, true); //the true will append the new data
            // fw.write("\n[User] " + message);
            // fw.close();
        } catch (Exception e) {
            System.err.println("Error with writing logging file. " + e.getMessage());
        }

    }

    /**
     * logs error and failure messages.
     */
    public static void logErrorMessage(String message) {
        try {
            // FileWriter fw = new FileWriter(f, true); //the true will append the new data
            // fw.write("\n[Error] " + message);
            // fw.close();
        } catch (Exception e) {
            System.err.println("Error with writing logging file. " + e.getMessage());

        }

    }

    /**
     * logs other messages, that don't fit into a category.
     */
    public static void logOtherMessage(String type, String message) {

        try {
            // FileWriter fw = new FileWriter(f, true); //the true will append the new data
            // fw.write("\n[" + type + "] " + message);
            // fw.close();
        } catch (Exception e) {
            System.err.println("Error with writing logging file. " + e.getMessage());
        }

    }

    /**
     * create a message window, for GUI based notices.
     */
    public static void messageWindow(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * Displays a error window.
     */
    public static void errorWindow(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }
}